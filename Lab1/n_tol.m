data=csvread('bisection_poly_B.csv'); % reading csv file 
X = data(:,1);
Y = data(:,2); 
figure;
hold on;
grid on;
plot(X, Y);
legend('Bisection method');
title("$\bf Tolerance(Iteration) \; 0.25 \cdot x^3 + x - 1.2502 = 0$", 'Interpreter','latex');
xlabel('Iteration')
ylabel('Tolerance')

data=csvread('secant_poly_B.csv'); % reading csv file 
X = data(:,1);
Y = data(:,2); 
figure;
hold on;
grid on;
plot(X, Y);
legend('Secant method');
title("$\bf Tolerance(Iteration) \; 0.25 \cdot x^3 + x - 1.2502 = 0$", 'Interpreter','latex');
xlabel('Iteration')
ylabel('Tolerance')

data=csvread('bisection_alg_B.csv'); % reading csv file 
X = data(:,1);
Y = data(:,2); 
figure;
hold on;
grid on;
plot(X, Y);
legend('Bisection method');
title("$\bf Tolerance(Iteration) \; x^4 - 3 \cdot x^3 - 9 \cdot x - 9 = 0$", 'Interpreter','latex');
xlabel('Iteration')
ylabel('Tolerance')

data=csvread('secant_alg_B.csv'); % reading csv file 
X = data(:,1);
Y = data(:,2); 
figure;
hold on;
grid on;
plot(X, Y);
legend('Secant method');
title("$\bf Tolerance(Iteration) \; x^4 - 3 \cdot x^3 - 9 \cdot x - 9 = 0$", 'Interpreter','latex');
xlabel('Iteration')
ylabel('Tolerance')

data=csvread('bisection_algbrake_B.csv'); % reading csv file 
X = data(:,1);
Y = data(:,2); 
figure;
hold on;
grid on;
plot(X, Y);
legend('Bisection method');
title("$\bf Tolerance(Iteration) \; -\frac{x^4 + 3 \cdot x^3 - 9 \cdot x - 9}{x - 1.5} = 0$", 'Interpreter','latex');
xlabel('Iteration')
ylabel('Tolerance')

data=csvread('secant_algbrake_B.csv'); % reading csv file 
X = data(:,1);
Y = data(:,2); 
figure;
hold on;
grid on;
plot(X, Y);
legend('Secant method');
title("$\bf Tolerance(Iteration) \; -\frac{x^4 + 3 \cdot x^3 - 9 \cdot x - 9}{x - 1.5} = 0$", 'Interpreter','latex');
xlabel('Iteration')
ylabel('Tolerance')