#include<stdio.h>
#include<stdlib.h>
#include<math.h>

typedef struct {
    double value, tolerance;
    int iteraction;
} solution;


double poly(double x) {
	return 0.25 * pow(x,3) + x - 1.2502;
}


double alg(double x) {
	return pow(x,4) + 3 * pow(x,3) - 9 * x - 9;
}


double algbrake(double x) {
	return -(pow(x,4) + 3 * pow(x,3) - 9 * x - 9)/(x-1.5);
}

double secant(double(*f)(double), double xk, double xk1, double eps, int N) {
	double xk2;
	double fxk = f(xk);
	double fxk1 = f(xk1);
	for (int i = 1; i < N; i++)
	{
		if (fabs(xk1 - xk) < eps) {
			break;
		}
		xk2 = xk1 - fxk1 * ((xk1 - xk) / (fxk1 - fxk));
		xk = xk1;
		fxk = fxk1;
		xk1 = xk2;
		fxk1 = f(xk2);
	}
	return xk2;
}

solution secant_solution(double(*f)(double), double xk, double xk1, double eps, int N) {
	double xk2;
	double fxk = f(xk);
	double fxk1 = f(xk1);
    solution sol;
    double secant_sol = secant(f, xk, xk1, pow(10, -15), 1000);

	for (int i = 1; i < N+1; i++)
	{
        sol.iteraction = i;
		if (fabs(xk1 - xk) < eps) {
			break;
		}
		xk2 = xk1 - fxk1 * ((xk1 - xk) / (fxk1 - fxk));
        sol.value = xk2;
        sol.tolerance = fabs(xk2 - secant_sol);
		xk = xk1;
		fxk = fxk1;
		xk1 = xk2;
		fxk1 = f(xk2);
	}
	return sol;
}

void SolutionSecantComparison(double(*f)(double), double a, double b, double eps, int N, char const *filename) {
	solution solu;
	FILE* file;
	file = fopen(filename, "w+");
	for (int i = 1; i < N; i++) {
		solu = secant_solution(f, a, b, eps, i);
		fprintf(file, "%d, %e, %lf\n", solu.iteraction, solu.tolerance, solu.value);
		if (solu.tolerance < eps) {
			break;
		}
	}
	fclose(file);
}


double bisection(double(*f)(double), double a, double b, double eps, int N) {
	double fa = f(a);
	double fb = f(b);
	double c;
	if (fa * fb >= 0) {
		return 0;
	}
	for (int i = 0; i < N; i ++) 
	{   
		c = 0.5 * (a + b);

		if ((b - a) < 2*eps) {
			return c;
		}

		double fc = f(c);

		if (fa * fc > 0) {
			a = c;
			fa = fc;
		}

		else {
			b = c;
			fb = fc;
		}
	}
	return c;
}

solution bisection_solution(double(*f)(double), double a, double b, double eps, int N) {
	double fa = f(a);
	double fb = f(b);
	double c;
    solution sol;
    double bisection_sol = bisection(f, a, b, pow(10, -15), 1000);

	for (int i = 1; i < N+1; i ++) 
	{   
        sol.iteraction = i;
		c = 0.5 * (a + b);
        sol.value = c;

		if ((b - a) < 2*eps) {
			return sol;
		}

		double fc = f(c);

		if (fa * fc > 0) {
			a = c;
			fa = fc;
		}

		else {
			b = c;
			fb = fc;
		}

        sol.tolerance = fabs(c - bisection_sol);
	}
	return sol;
}

void SolutionBisectionComparison(double(*f)(double), double a, double b, double eps, int N, char const *filename) {
	solution solu;
	FILE* file;
	file = fopen(filename, "w+");
	for (int i = 1; i < N; i++) {
		solu = bisection_solution(f, a, b, eps, i);
		fprintf(file, "%d, %e, %lf\n", solu.iteraction, solu.tolerance, solu.value);
		if (solu.tolerance < eps) {
			break;
		}
	}
	fclose(file);
}



void PrintResultSolution(solution result) {
    printf("Value:     %.10lf\n", result.value);
    printf("Tolerance: %.10lf\n", result.tolerance);
    printf("Iteration: %d\n", result.iteraction);
}

void PrintResult (double eps) {
    solution x1 = secant_solution(algbrake, 1.51, 2, eps, 1000);
	solution x2 = bisection_solution(algbrake, 1.51, 2, eps, 1000);
    printf("Function with a brake: \n");
    printf("Secant method: \n");
	PrintResultSolution(x1);
	printf("Bisection method: \n");
	PrintResultSolution(x2);
	
    solution x3 = secant_solution(alg, 1, 2, eps, 1000);
	solution x4 = bisection_solution(alg, 1, 2, eps, 1000);
    printf("Algebraic function: \n");
    printf("Secant method: \n");
	PrintResultSolution(x3);
	printf("Bisection method: \n");
	PrintResultSolution(x4);

    solution x5 = secant_solution(poly, 0, 2, eps, 1000);
	solution x6 = bisection_solution(poly, 0, 2, eps, 1000);
    printf("Polynomial: \n");
    printf("Secant method: \n");
	PrintResultSolution(x5);
	printf("Bisection method: \n");
	PrintResultSolution(x6);
}





int main() {
	PrintResult(pow(10, -10));
    printf("\n\n");
    PrintResult(2*pow(10, -11));
}
